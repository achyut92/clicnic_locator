package com.example.a0134598r.pathfinder.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0134598r.pathfinder.R;
import com.example.a0134598r.pathfinder.models.Clinic;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class FindClinicEstateName extends ActionBarActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.


    //private String API_KEY = getResources().getString(R.string.google_maps_key);

    private String API_KEY = "AIzaSyAXNTxodHtPuBG0N54tgdZYRfNY2FRDej8";


    //hard cording coordinate

    private static double LAT = 1.3161811,
            LNG = 103.7649377;



    String estateName;
    String type = "hospital";

    ArrayList<Clinic> result;
    int mark = 0;


    //display pics in InfoWindow
   // private ImageLoader imageLoader;
    //private DisplayImageOptions options;
    private Marker marker;
    public CustomInfoWindowAdapter adapter;

    private HashMap<Integer, String> dayMap = new HashMap<>();
    private Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Asia/Singapore"),
            Locale.ENGLISH);
    private String operatingDay;
    public int aviva;
    public Clinic clinic;

    ArrayList<Integer>avivaList;
    int avivaListIndex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_direction3);

        //result = new ArrayList<Clinic>();
        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        Intent i = getIntent();
        estateName = i.getStringExtra("estate_name");

        setUpMapIfNeeded();
        setPreference();

        retrieveFromCloudByEstateName(estateName);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(android.os.Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.

        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();

            adapter = new CustomInfoWindowAdapter();
            populateDayMap();
            this.operatingDay = dayMap.get(Integer.valueOf(c.get(Calendar.DAY_OF_WEEK)));
           // c.set(Calendar.HOUR_OF_DAY,15);

            // in get places.
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

        if (mMap != null) {
            mMap.setInfoWindowAdapter(adapter);
            mMap.addMarker(new MarkerOptions().position(new LatLng(LAT, LNG)).title("Marker"));
        }
            //geoLocate();

    }



    private void setPreference() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }

    private void populateDayMap() {
        dayMap.put(2, "MON");
        dayMap.put(3, "TUE");
        dayMap.put(4, "WED");
        dayMap.put(5, "THU");
        dayMap.put(6, "FRI");
        dayMap.put(7, "SAT");
        dayMap.put(1, "SUN");
    }

    private void retrieveFromCloudByEstateName(String estateName) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Clinic");
        query.whereEqualTo("ESTATE", estateName);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                FindClinicEstateName.this.result = new ArrayList<Clinic>();
                FindClinicEstateName.this.avivaList = new ArrayList<Integer>();
                boolean finalJudge = false;
                boolean tempJudge = false;
                if (objects.size() >= 1) {
                    for (ParseObject po : objects) {
                        FindClinicEstateName.this.clinic = new Clinic(po.getString("CLINIC"),po.getString("ADDRESS_1"),po.getString("ESTATE"),po.getDouble("LATITUDE"),
                                po.getDouble("LONGITUDE"),String.valueOf(po.getInt("AVIVA_CODE")));

                        FindClinicEstateName.this.aviva = po.getInt("AVIVA_CODE");
                        FindClinicEstateName.this.avivaList.add(FindClinicEstateName.this.aviva);

                        result.add(clinic);
                        //Log.i("uuu", po.getString("CLINIC"));
                    }


                }else{
                    Toast.makeText(getApplicationContext(),"Can't find Clinics",Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(FindClinicEstateName.this, SearchScreen.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
                    startActivity(myIntent);
                    finish();
                }

                for (avivaListIndex = 0; avivaListIndex < FindClinicEstateName.this.avivaList.size(); avivaListIndex++) {
                    retriveFromCloudByAllClinicTiming(avivaList.get(avivaListIndex),avivaListIndex);

                }
            }

        });
    }

    private void retriveFromCloudByAllClinicTiming(final int aviva,final int index){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("ClinicTimings");
        query.whereEqualTo("OPERATING_DAY", operatingDay);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                //ArrayList<ClinicTiming> clinicTimings = new ArrayList<ClinicTiming>();
                boolean tempJudge = false;
                boolean finalJudge = false;
                if (list != null) {
                    for (ParseObject clObject : list) {
                        int code = clObject.getInt("AVIVA_CODE");
                        if (code == aviva) {
                            String oh_start = clObject.getString("OPERATING_HOURS_START");
                            String oh_end = clObject.getString("OPERATING_HOURS_END");
                            //Log.i("-->> Aviva Code: ", code + " Operating hrs: "
                            //        + oh_start + " - " + oh_end + " "+c.getTime().getHours());
                            Log.i("ttt", code + " Operating hrs: "
                                    + oh_start + " - " + oh_end + " " + operatingDay);
                            try {
                                tempJudge = isOpen(oh_start,oh_end);
                            } catch (java.text.ParseException e1) {
                                e1.printStackTrace();
                            }

                            if (tempJudge == true){
                                finalJudge = true;
                                break;
                            }

                        }
                    }
                } else {
                    Log.i("Error: ", "Could not retrieve data!!!");
                }
                //clinic.setClinicTimings(clinicTimings);
                for (int i = 0 ; i < result.size() ; i ++) {
                    if (Integer.valueOf(result.get(i).getAVIVA_CODE()) == aviva) {
                        result.get(i).setOpen(finalJudge);
                    }
                }


                if (avivaList.size() == result.size()){
                    avivaListIndex = 0;
                    if (mMap == null) {
                        mMap = FindClinicEstateName.this.mMap;
                    }
                    //Toast.makeText(getApplicationContext(),String.valueOf(result.size()) , Toast.LENGTH_LONG).show();


                    try {
                        setMarker(mMap,result);
                    } catch (java.text.ParseException e1) {
                        e1.printStackTrace();
                    }


                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(result.get(0).getLATITUDE(), result
                                    .get(0).getLONGITUDE())) // Sets the center of the map to
                                    // Mountain View
                            .zoom(14) // Sets the zoom
                            .tilt(30) // Sets the tilt of the camera to 30 degrees
                            .build(); // Creates a CameraPosition from the builder
                    mMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                }
            }
            private void setMarker(GoogleMap map,ArrayList<Clinic> result) throws java.text.ParseException {


                for (int i = 0; i < result.size(); i++) {

                    LatLng PERTH = new LatLng(result.get(i).getLATITUDE(), result
                            .get(i).getLONGITUDE());

                    if (result.get(i).isOpen()){
                        map.addMarker(new MarkerOptions()
                                .title(result.get(i).getCLINIC())
                                .position(PERTH)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                .snippet(result.get(i).getADDRESS_1()));

                    }else {
                        map.addMarker(new MarkerOptions()
                                .title(result.get(i).getCLINIC())
                                .position(PERTH)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .snippet(result.get(i).getADDRESS_1()));
                    }
                }
            }

        });


    }

    public boolean isOpen(String oh_start, String oh_end) throws java.text.ParseException {

        /*
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date dateStart = formatter.parse(oh_start);
        Date dateEnd = formatter.parse(oh_end);
        */

        String[] dateStart = oh_start.split(":");
        String dateStartHour = dateStart[0]; // 004
        String dateStartMin = dateStart[1];

        String[] dateEnd = oh_end.split(":");
        String dateEndHour = dateEnd[0]; // 004
        String dateEndMin = dateEnd[1];

        int start = Integer.valueOf(dateStartHour) * 60 + Integer.valueOf(dateStartMin);
        int end = Integer.valueOf(dateEndHour)*60 + Integer.valueOf(dateEndMin);

        int now = FindClinicEstateName.this.c.getTime().getHours() * 60 + FindClinicEstateName.this.c.getTime().getMinutes();

        Log.i("ttt",String.valueOf(start)+"  "+String.valueOf(now)+"  "+String.valueOf(end));

        if (now > start && now < end){
            return true;
        }else{
            return false;
        }
    }


    private void setupCustomInfoWindow() {

        if (mMap != null) {
            mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        }
    }


    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;
        private ImageView image;
        private boolean retrieveSwitch;

        public CustomInfoWindowAdapter() {
            view = getLayoutInflater().inflate(R.layout.custom_info_window,
                    null);
            image = ((ImageView) view.findViewById(R.id.badge));
            retrieveSwitch = false;
        }

        @Override
        public View getInfoContents(Marker marker) {

            if (FindClinicEstateName.this.marker != null
                    && FindClinicEstateName.this.marker.isInfoWindowShown()) {
                FindClinicEstateName.this.marker.hideInfoWindow();
                FindClinicEstateName.this.marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            FindClinicEstateName.this.marker = marker;

            //retrieve pics
            FindClinicEstateName.this.adapter.retrieveImage(marker.getTitle());
            Log.i("rr","how many times?");

            final String title = marker.getTitle();
            final TextView titleUi = ((TextView) view.findViewById(R.id.title));
            if (title != null) {
                titleUi.setText(title);
            } else {
                titleUi.setText("");
            }

            final String snippet = marker.getSnippet();
            final TextView snippetUi = ((TextView) view
                    .findViewById(R.id.snippet));
            if (snippet != null) {
                snippetUi.setText(snippet);
            } else {
                snippetUi.setText("");
            }

            getInfoContents(marker);
            return view;
        }

        public void retrieveImage(String clinicName){

            ParseQuery<ParseObject> query = ParseQuery.getQuery("ClinicListing");
            query.whereEqualTo("CLINIC", clinicName);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (object != null) {
                        ParseFile file = (ParseFile)object.get("Insurance");
                        //view = (ImageView) findViewById(R.id.imageView1);
                        file.getDataInBackground(new GetDataCallback() {
                        ImageView someview= CustomInfoWindowAdapter.this.image;
                        public void done(byte[] data, ParseException e) {
                                if (data.length != 0) {

                                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                    //use this bitmap as you want
                                    someview.setImageBitmap(bitmap);

                                } else {
                                    // something went wrong
                                    Bitmap bitmap = BitmapFactory.decodeFile("/sdcard/Pictures/null.png");
                                    //use this bitmap as you want
                                    someview.setImageBitmap(bitmap);
                                }
                            }
                        });

                    } else {
                        Toast.makeText(getApplicationContext(), "Exception", Toast.LENGTH_SHORT) .show();

                    }
                }
            });

        }
    }
}
